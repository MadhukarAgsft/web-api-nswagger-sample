# core-api-client-generator
 * Application to create API client  code using the swagger specification json file(url) created using NSwag
 * Before ASP.net Web Api, to consume WCF we do `Service References` in WCF client apps and which was generating us a `serviceClient` class which will have methods for all WCF service's OperationContracts. 
* Then we consume WCF service methods is easy as we just create an object for `serviceClient` and call methods that we need.
* But  in any C# server side applications, if we need to consume Apis created using Web API then we write the code using `HttpClient` OR `WebClient` and serializing/deserializng the data that we are sending/retrieving.
*  To Avoid this manual coding and to genreate Code like WCF Service's `ServiceClient` we use Nswag.

 
# OpenAPI (Swagger) Connected Service
* In this project we are generating Client code using OpenAPI (Swagger) Connected Service (https://marketplace.visualstudio.com/items?itemName=dmitry-pavlov.OpenAPIConnectedService)

# How to install 
* Goto `Visual Studio` --> `Tools` --> `Extensions and Updates` and search for `OpenAPI (Swagger) Connected Service` and `Install`
* Close `all Visual studio` instances opened. It was just schdule the installation but not installed, need to close all Visual studio instances to finish the installation.
* Re-open `Visual studio` and verify its installed (Goto `Tools` --> `Extensions and Updates` and Expand 'Installed' (under left panel) -> click on `Tools` --> will list down `OpenAPI (Swagger) Connected Service` 

# How to use it
* Create a C# Console app
* RightClick on Project Name -> `Add` --> `Connected Service` --> click on `OpenAPI (Swagger) Connected Service` --> in `URI` textbox enter your swagger specification json url http://localhost:4200/swagger/v1/swagger.json and in `Name` textbox enter your API name like ProductsApi, ProjectManagerApi etc and `Finish`

* a `gif` that explains the above process

![Nswag Generator](https://dmitry-pavlov.gallerycdn.vsassets.io/extensions/dmitry-pavlov/openapiconnectedservice/1.2.2/1558640988642/OpenAPI-Swagger-Connected-Service.gif "NSwag generator")

* Now, in root location of your Console app will have folder `ProducstsApi` with below files
*  `ProducstsApiClient.Generated.cs` - Client code will be generated like Api response Entity classes, interfaces, classes for API methods
*  `ProducstsApiClient.nswag.json`  - your swagger spec json that used to generate client code
*  `ProducstsApiClient.nswag`  - NSwag settings that used for genrating the Client code. First time we have no option to set the settings (in the above process). But, once you generated code then you can open this file(.nswag) and can edit settings(in json format) as you needed.<br/>
Eg: when you generate first time you will have client Class but there will be no Interface. But if you want an interface with your APIs  methods and your Client class will inherit from that interface and have implementation (of code to consume your API methods), then change `"generateClientInterfaces": false,` in `.nswag` file  to `      "generateClientInterfaces": true,`
* See the full json [here](https://github.com/madhukar-dotnet/core-api-client-generator/blob/master/api-client-generator/api-client-generator/Connected%20Services/ApiClient/ApiClient.nswag) and use `swaggerToCSharpClient` json property contains all the settings that you can update
* Once you updte the `.nswag` json file then right click on your Connected service folder name `ProductApi` (folder `ApiClient` in this project)  --> `Update OpenAPI (Swagger) Connected Service` --> Enter URI, Name (same Name that used previosuly should be manually entered) and URI of swagger json --> `Finish`
* Now if you open your `ProducstsApiClient.Generated.cs` you will see interfaces with methods and classes implementing those methods by inheriting from interfaces.

* Now you can use the genreated code to call your APIs in any of your C# server side code
    <br /> `var employees = new Employeeclient(_httpClient).GetAsync(10).Result; // if you don't want to send HttpClient object then you should have genreated your code by setting   "injectHttpClient": false, in your  .nswag file` 

* All generated classes are partial classes so if you want to extend any functionality after your code genration then you can create another file with same partial classs names (but you can't edit these auto genrated files which may  overrid your changes in future).

# Prior to NSwag
 Before Nswag,  
* open source platform  [Swashbuckle](https://github.com/domaindrivendev/Swashbuckle) to genreate API documentation using Swagger spec (both json and UI ) 
* [Auto REST](https://github.com/Azure/autorest) which used to genrate the Client code (for different types languages) using swagger.json (that we genreated sing SwashBuckle)

* So, Nswag  = SwashBuckle  +  AutoRest

# What if you want to integrate your API in Angular app
* we have other Connected service `Unchase` which also can done in the same above process and can gerneate client code for Typescript ( see the below )

![TypescriptClient code for Json API](https://miro.medium.com/max/1724/1*K-rsP-whOvkEpbRuAasyZA.gif "TypescriptClient code for Json API")



*
