﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCore.Api.Sample.Models
{
    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }

}
