﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetCore.Api.Sample.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DotNetCore.Api.Sample.Controllers
{
    [Route("api/[controller]/{id?}")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        [ProducesResponseType(typeof(Employee), 200)]
        [ProducesResponseType(500)]
        [ProducesResponseType(400)]
        [HttpGet]
        public IActionResult Get(int i)
        {
            try
            {
                if (i <= 0)
                {
                    return BadRequest("invalid employee Id passed");
                }
                return Ok(new Employee { Id= 1, Name= "Only Madhukar"});
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}