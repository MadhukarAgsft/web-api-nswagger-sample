# core-api-sample
Asp.net Core Sample Api with NSwag for API documentation

# API documentation

* API documentation is a technical content deliverable, containing instructions about how to effectively use and integrate an API. 
* It's a concise reference manual containing all the information required to work with the API, with details about the functions, classes, return types, arguments.
* API Documentation has traditionally been done using regular content creation tools aka text editors.

# Automation of API Documentation
* Now we can automate APi documenation using the API specfications(https://swagger.io/specification/) to make it easier for developers to generate and maintain

# NSwag
* Nswag is used to provide API documenation for your Asp.net Web API and also we can generate the Client code for your API and This API client  code can be used to intergrate your Web API in C# (server-side code) to avoid writing code manually with HttpClient

* Install Nswag
  `Install-Package NSwag.AspNetCore`
* In the `Startup.ConfigureServices` method, register the required Swagger services:
```
public void ConfigureServices(IServiceCollection services)
{
    // Register the Swagger services
    services.AddSwaggerDocument();
}
```
* In the `Startup.Configure` method, enable the middleware for serving the generated Swagger specification and the Swagger UI:
```
public void Configure(IApplicationBuilder app)
{
    // Register the Swagger generator and the Swagger UI middlewares
    app.UseOpenApi();
    app.UseSwaggerUi3();
}
```
* Run your API. Assuming it opened on http://localhost:4200 
* To view the Swagger UI type the below url in your browser.
  http://localhost:4200/swagger 
* To view the Swagger specifications json, type the below url in your browser.
  http://localhost:4200/swagger/v1/swagger.json

# Reference
https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-nswag?view=aspnetcore-3.0&tabs=visual-studio

# NSwag specific API developement
* Wrtiting the `ProducesResponseType` Attribute so that NSwag will read it and the Response Type (an Entity or Error Status Code) of your API in spec file.
```
 public class EmployeeController : ControllerBase
    {
        [ProducesResponseType(typeof(Employee), 200)]
        [ProducesResponseType(500)]
        [ProducesResponseType(400)]   
        [HttpGet]
        public IActionResult Get(int i)
        {
            try
            {
                if (i <= 0)
                {
                    return BadRequest("invalid employee Id passed"); // return 400
                }
                return Ok(new Employee { Id= 1, Name= "Only Madhukar"});
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
```

# How to genrate Client Code for API
Implemented in another project https://bitbucket.org/MadhukarAgsft/web-api-nswagger-sample/src/master/core-api-client-generator/
